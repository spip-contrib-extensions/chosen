<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-chosen?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chosen_description' => '[Chosen->http://harvesthq.github.com/chosen/] è una libreria JavaScript che migliora l’esperienza utente dei selettori nei moduli HTML.

La classe CSS <code>chosen</code> su un <code><select></code> caricherà automaticamente Chosen su di essa.
Questo ramo del plugin Chosen è basato sulla forchetta di koenpunt - versione 1.0.0 - vedi https://github.com/koenpunt/chosen/releases/.

Con questa forchetta, Chosen consente di <a href=\'https://github.com/harvesthq/chosen/pull/166\'> creare nuove opzioni</a> in un &lt;select&gt; esistente, a condizione che abbia la classe "chosen-create-option".
Quando viene scelto crea una nuova &lt;option&gt; (es. la parola "nuovo) in un &lt;select&gt;, questo assume la forma seguente: <code>&lt;option selected=\'selected\' value=\'chosen_nouveau\'&gt;nouveau&lt;/option&gt;</code>.
Nota il prefisso "chosen_" aggiunto nel parametro "value" per permettere la differenziazione tra &lt;option&gt; create da Chosen.',
	'chosen_nom' => 'Chosen (forchetta di koenpunt)',
	'chosen_slogan' => 'Integra la libreria Chosen dentro SPIP (forchetta di koenpunt)'
);
