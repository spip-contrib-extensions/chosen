<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/chosen?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_exemple' => 'Esempio',
	'cfg_exemple_explication' => 'Spiegazione dell’esempio',
	'cfg_titre_parametrages' => 'Impostazioni',
	'chosen_titre' => 'Chosen',

	// E
	'explication_selecteur' => 'Indicare il target degli elementi che attiveranno chosen. (espressione CSS o estensione jQuery)',

	// L
	'label_active' => 'Attivare "Chosen" sul sito pubblico',
	'label_selecteur' => 'Selettore',
	'lang_create_option' => 'Creare',
	'lang_no_result' => 'Nessun risultato',
	'lang_select_an_option' => 'Selezionare un’opzione',
	'lang_select_some_option' => 'Selezionare una o più opzioni',

	// T
	'titre_page_configurer_chosen' => 'Configura "Chosen"'
);
